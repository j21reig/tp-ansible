#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <destination>"
    echo "  destination: cart, frontend, productCatalog, recommendation"
    exit 1
fi

dest=$1

if [ $dest = "cart" ]; then
    ssh root@10.29.244.15 -p 22
elif [ $dest = "frontend" ]; then
    ssh root@10.29.246.97 -p 22
elif [ $dest = "productCatalog" ]; then
    ssh root@10.29.247.194 -p 22
elif [ $dest = "recommendation" ]; then
    ssh root@10.29.244.83 -p 22
fi